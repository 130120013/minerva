#include "../include/io/minerva_iostream.hpp"
#include <iostream>

int main() 
{
    auto file = load_file("/home/alina/protege/AcousticsMethods.owl");
    std::cout << "Loaded file size: " << file.size() << "\n";
    return 0;
}
