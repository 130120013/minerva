#include "../io/minerva_iostream.hpp"
#include "plog/Initializers/RollingFileInitializer.h"

std::string load_file(std::string path) 
{
    std::string file_data = "";
    plog::init(plog::debug, "IOLog.txt");
    PLOGD << "Start loading file from path: " << path << "\n";              
    try 
    {
        std::ifstream ifs(path);
        file_data = std::string((std::istreambuf_iterator<char>(ifs)),
            std::istreambuf_iterator<char>());
        ifs.close();
    }
    catch(std::exception& e)
    {
        PLOGD << "Error occurs: " << e.what() << "\n";              
    }
    return file_data;
}

std::string load_from_uri(std::string uri) 
{
    return "";
}
