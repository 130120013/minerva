#ifndef MINERVA_IOSTREAM_HPP
#define MINERVA_IOSTREAM_HPP

#include "../plog/include/plog/Log.h"
#include <cstring>
#include <exception>
#include <iterator>
#include <fstream>

std::string load_file(std::string path);
std::string load_from_uri(std::string uri);
#endif

